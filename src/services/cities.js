const moment = require('moment')
const path = require('path')
const base = require('./base')

const citiesFile = path.resolve(__dirname, '..', 'data', 'city_list.json')
const weatherFile = path.resolve(__dirname, '..', 'data', 'weather_list.json')

const dateToUnixTimestamp = date => {
  const unix = moment(date).unix()
  return unix
}

const compareTwoArrays = (arr1, arr2) => {
  const response = []
  for (let i = 0; i < arr1.length; i++) {
    for (let j = 0; j < arr2.length; j++) {
      if (arr1[i].id === arr2[j].cityId) {
        response.push({ city: arr1[i], weather: arr2[j] })
      }
    }
  }

  return response
}

const allCities = async () => {
  const cities = await base(citiesFile)
  return cities
}

const allCitiesWithWeather = async () => {
  const cities = await base(citiesFile)
  const weather = await base(weatherFile)

  const citiesWithWeather = compareTwoArrays(cities, weather)
  return citiesWithWeather
}

const showCityWithWeather = async city => {
  const cities = await base(citiesFile)
  const weather = await base(weatherFile)

  const citiesWithWeather = compareTwoArrays(cities, weather)

  const response = citiesWithWeather.filter(c => {
    return c.city.name === city
  })
  return response
}

const showCityWithWeatherWithRangeFilter = async (
  city,
  initialDate,
  finalDate
) => {
  const cities = await base(citiesFile)
  const weather = await base(weatherFile)

  const cityID = cities.filter(c => {
    return c.name === city
  })

  if (!cityID) {
    return []
  }

  const weatherByCityID = weather.filter(w => {
    return w.cityId === cityID[0].id
  })

  const weatherRangeFilter = weatherByCityID[0].data.filter(w => {
    return (
      w.dt >= dateToUnixTimestamp(initialDate) &&
      w.dt <= dateToUnixTimestamp(finalDate)
    )
  })

  const response = [
    {
      city: cityID[0],
      weather: weatherRangeFilter
    }
  ]

  return response[0]
}

module.exports = {
  allCities,
  allCitiesWithWeather,
  showCityWithWeather,
  showCityWithWeatherWithRangeFilter
}
