const { promisify } = require('util')
const { readFile } = require('fs')

const readFiles = promisify(readFile)

const getData = async dataFile => {
  try {
    const data = await readFiles(dataFile, 'utf-8')
    return JSON.parse(data)
  } catch (error) {
    console.error(error)
  }
}

module.exports = getData
