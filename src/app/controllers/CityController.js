const {
  allCities,
  allCitiesWithWeather,
  showCityWithWeather,
  showCityWithWeatherWithRangeFilter
} = require('../../services/cities')

module.exports = {
  allCities: async (req, res) => {
    const cities = await allCities()
    res.json({
      data: cities
    })
  },

  allCitiesWithWeather: async (req, res) => {
    const cities = await allCitiesWithWeather()
    res.json({
      data: cities
    })
  },

  showCitiesWithWeather: async (req, res) => {
    const city = await showCityWithWeather(req.params.city)
    res.json({
      data: city
    })
  },

  showCityWithWeatherFilter: async (req, res) => {
    const { initiaDate, finalDate } = req.query
    const city = await showCityWithWeatherWithRangeFilter(
      req.params.city,
      initiaDate,
      finalDate
    )
    res.json({
      data: city
    })
  }
}
