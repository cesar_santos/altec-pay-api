const routes = require('express').Router()

const CityController = require('../app/controllers/CityController')

/**
 * @api {Get} /cities Get all cities
 * @apiName GetCities
 * @apiGroup Cities
 * @apiSuccess {Object} data returns an array of objects with cities
 * @apiSuccessExample {json} Success
 *  HTTP/1.1  200 OK
 *  {"data": [
  *  {
        "id": 3531732,
        "coord": {
        "lon": -90.533333,
        "lat": 19.85
      },
        "country": "MX",
        "geoname": {
        "cl": "P",
        "code": "PPLA",
        "parent": 6942858
      },
        "name": "Campeche",
        "stat": {
        "level": 1,
        "population": 205212
      },
        "stations": [
        {
          "id": 3968,
          "dist": 4,
          "kf": 1
        }
      ],
        "zoom": 7
      }
 *  ]}
 */
routes.get('/cities', CityController.allCities)

/**
 * @api {get} /cities/weather Get All Cities with Weather
 * @apiGroup Cities
 * @apiSuccess {Object} data returns an array of objects with cities and weather
 * @apiSuccessExample {json} Sucesso
 *  HTTP/1.1  200 OK
 *  {"data": [
 *    city: [],
 *    weather: []
 * ]}
 */
routes.get('/cities/weather', CityController.allCitiesWithWeather)

/**
 * @api {get} /cities/:city/weather Get City
 * @apiGroup Cities
 * @apiParam {String} city
 * @apiSuccess {Object} data returns an array of objects with city and weather
 * @apiSuccessExample {json} Sucesso
 *  HTTP/1.1  200 OK
 *  {"data": [
 *
 *  ]}
 */
routes.get('/cities/:city/weather', CityController.showCitiesWithWeather)

/**
 * @api {get} /cities/:city/weather/filter?initialDate=initialDate&finalDate=finalDate Get city with weather
 * @apiGroup Cities
 * @apiParam {String} city
 * @apiParam {Date} initialDate 2017-03-01
 * @apiParam {Date} finalDate 2017-10-20
 * @apiSuccess {Object} get city with weather
 * @apiSuccessExample {json} Sucesso
 *  HTTP/1.1  200 OK
 *  {"data": [
 *    city: [],
 *    weateher : []
 *  ]
 */
routes.get('/cities/:city/filter', CityController.showCityWithWeatherFilter)

module.exports = routes
