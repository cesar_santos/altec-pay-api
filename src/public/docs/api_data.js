define({ "api": [
  {
    "type": "Get",
    "url": "/cities",
    "title": "Get all cities",
    "name": "GetCities",
    "group": "Cities",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>returns an array of objects with cities</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1  200 OK\n{\"data\": [\n{\n       \"id\": 3531732,\n       \"coord\": {\n       \"lon\": -90.533333,\n       \"lat\": 19.85\n     },\n       \"country\": \"MX\",\n       \"geoname\": {\n       \"cl\": \"P\",\n       \"code\": \"PPLA\",\n       \"parent\": 6942858\n     },\n       \"name\": \"Campeche\",\n       \"stat\": {\n       \"level\": 1,\n       \"population\": 205212\n     },\n       \"stations\": [\n       {\n         \"id\": 3968,\n         \"dist\": 4,\n         \"kf\": 1\n       }\n     ],\n       \"zoom\": 7\n     }\n]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/routes/routes.js",
    "groupTitle": "Cities"
  },
  {
    "type": "get",
    "url": "/cities/:city/weather",
    "title": "Get City",
    "group": "Cities",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>returns an array of objects with city and weather</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucesso",
          "content": "HTTP/1.1  200 OK\n{\"data\": [\n\n]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/routes/routes.js",
    "groupTitle": "Cities",
    "name": "GetCitiesCityWeather"
  },
  {
    "type": "get",
    "url": "/cities/:city/weather/filter?initialDate=initialDate&finalDate=finalDate",
    "title": "Get city with weather",
    "group": "Cities",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "initialDate",
            "description": "<p>2017-03-01</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "finalDate",
            "description": "<p>2017-10-20</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "get",
            "description": "<p>city with weather</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucesso",
          "content": "HTTP/1.1  200 OK\n{\"data\": [\n  city: [],\n  weateher : []\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/routes/routes.js",
    "groupTitle": "Cities",
    "name": "GetCitiesCityWeatherFilterInitialdateInitialdateFinaldateFinaldate"
  },
  {
    "type": "get",
    "url": "/cities/weather",
    "title": "Get All Cities with Weather",
    "group": "Cities",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>returns an array of objects with cities and weather</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucesso",
          "content": " HTTP/1.1  200 OK\n {\"data\": [\n   city: [],\n   weather: []\n]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/routes/routes.js",
    "groupTitle": "Cities",
    "name": "GetCitiesWeather"
  }
] });
