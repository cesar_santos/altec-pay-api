const request = require('supertest')
const app = require('../../src/app')

describe('Cities', () => {
  it('should list all cities', () => {
    return request(app)
      .get('/cities')
      .then(res => {
        expect(res.status).toBe(200)
      })
  })

  it('should list all cities with weather', () => {
    return request(app)
      .get('/cities/weather')
      .then(res => {
        expect(res.status).toBe(200)
      })
  })

  it('should list a city with climate by the name of the city', () => {
    return request(app)
      .get('/cities/Piedras Negras/weather')
      .then(res => {
        expect(res.status).toBe(200)
      })
  })

  it('should list a city with weather by city name and a range of dates', () => {
    return request(app)
      .get(
        '/cities/Piedras Negras/filter?initiaDate=2017-03-10&finalDate=2017-08-20'
      )
      .then(res => {
        expect(res.status).toBe(200)
      })
  })
})
